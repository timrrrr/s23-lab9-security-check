## Password Reset Functionality

Test steps and results:

| Test step                                                      | Result                     |
| -------------------------------------------------------------- | -------------------------- |
| Visit https://www.notion.so/login                              | Ok                         |
| Press on "Forgot password"                                     | Ok                         |
| Enter email for existing account and click "Send reset link"   | Ok                         |
| Check response message                                         | "Check your inbox for the link to reset your password." |
| Measure response time for existing email (using devtools)      | 459 ms, 574 ms, 835 ms (avg = 623 ms)     |
| Enter email for nonexistent account (e.g., dskahvgkj@mail.ru)  | Ok                         |
| Click "Send reset link"                                        | Ok                         |
| Check response message                                         | "We could not reach the email address you provided. Please try again with a different email." |
| Measure response time for nonexistent email (using devtools)   | 274 ms, 282 ms, 334 ms (avg = 297 ms)     |

### Summary

Now we can check which OWASP requirements the website follows

| Requirement                                                         | Result |
| ------------------------------------------------------------------- | ------ |
| Return a consistent message for both existent and non-existent accounts | -      |
| Ensure that the time taken for the user response message is uniform | -      |
| Use a side-channel to communicate the method to reset their password | + (restore link is sent to the email) |

### Inconsistent Messages for Existent and Non-Existent Accounts

During the testing, it was observed that the website does not follow the OWASP guideline of returning a consistent message for both existent and non-existent accounts when using the "Forgot Password" feature. The website reveals whether the provided email address is incorrect or non-existent, which could be a potential vulnerability. Attackers could potentially use this information to enumerate valid email addresses associated with accounts on the platform. As an alternative it can show a consistent message like "Check your inbox for the link to reset your password. If there is no email make sure the email is valid."

## Case-insensitive User IDs

Here is the test case:

| Test step                                             | Result |
| ----------------------------------------------------- | ------ |
| Have an existing account                              | My login is timur_rameev@mail.ru|
| Visit https://www.notion.so/login                     | Ok     |
| Enter "TIMUR_RAMEEV@mail.ru" as login and a valid password | Ok |
| Check that the authentication is successful           | Successful |
| Visit https://www.notion.so/signup                          | Ok     |
| Enter some username, some password, and email "TIMUR_RAMEEV@mail.ru" | Ok |
| Check that the website disallows repeating emails     | Password field appears for the existing email |

### Summary

Now we can check which OWASP requirements the website follows

| Requirement                               | Result |
| ----------------------------------------- | ------ |
| Usernames/user IDs should be case-insensitive | +      |
| Usernames should also be unique           | +      |
| Email addresses should be unique          | + (website detects the existing email and prompts for a password instead) |